package com.virginpulse

import ShoppingListItem
import io.ktor.application.*
import io.ktor.features.*
import io.ktor.http.*
import io.ktor.http.content.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.serialization.*
import io.ktor.server.netty.*
import org.slf4j.LoggerFactory

fun main(args: Array<String>) = EngineMain.main(args)


fun Application.main() {
    val logger = LoggerFactory.getLogger("Main")
    logger.info("Environment: $envKind")
    installFeatures()


    routing {
        index(shoppingListItemDao)
    }
}

fun Application.installFeatures() {
    install(ContentNegotiation) {
        json()
    }
    install(CORS) {
        method(HttpMethod.Get)
        method(HttpMethod.Post)
        method(HttpMethod.Delete)
        anyHost()
    }
    install(Compression) {
        gzip()
    }
    install(DefaultHeaders)
}


fun Route.index(dao: ShoppingListItemDao) {
    get("/") {
        call.respondText(
            this::class.java.classLoader.getResource("index.html")!!.readText(),
            ContentType.Text.Html
        )
    }

    static("/") {
        resources("")
    }

    route(ShoppingListItem.path) {
        get {
            call.respond(dao.getAllItems())
        }

        post {
            dao.insertShoppingListItem(call.receive())
            call.respond(HttpStatusCode.Created)
        }

        delete("/{id}") {
            val id = call.parameters["id"]?.toInt() ?: error("Invalid delete request")
            if (dao.deleteShoppingListItem(id)) {
                call.respond(HttpStatusCode.NoContent)
            } else {
                call.respond(HttpStatusCode.NotFound)
            }
        }

    }
}

val Application.envKind get() = environment.config.property("ktor.environment").getString()
val Application.isDev get() = envKind == "dev"
val Application.isProd get() = envKind != "dev"

