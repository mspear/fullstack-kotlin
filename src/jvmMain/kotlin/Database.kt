package com.virginpulse

import ShoppingListItem
import com.virginpulse.dynamo.DynamoDbScope
import io.ktor.application.*
import kotlinx.coroutines.future.await
import org.litote.kmongo.coroutine.coroutine
import org.litote.kmongo.eq
import org.litote.kmongo.reactivestreams.KMongo
import software.amazon.awssdk.services.dynamodb.DynamoDbAsyncClient
import software.amazon.awssdk.services.dynamodb.model.AttributeValue
import software.amazon.awssdk.services.dynamodb.model.ScanRequest
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

interface ShoppingListItemDao {
    suspend fun getAllItems(): List<ShoppingListItem>

    suspend fun deleteShoppingListItem(id: Int): Boolean

    suspend fun insertShoppingListItem(item: ShoppingListItem)
}

class MongoShoppingListItemDao : ShoppingListItemDao {
    private val collection by lazy {
        val client = KMongo.createClient().coroutine
        val database = client.getDatabase("shoppingList")
        database.getCollection<ShoppingListItem>()
    }

    override suspend fun getAllItems(): List<ShoppingListItem> {
        return collection.find().toList()
    }

    override suspend fun deleteShoppingListItem(id: Int): Boolean {
        return collection.deleteOne(ShoppingListItem::id eq id).deletedCount > 0
    }

    override suspend fun insertShoppingListItem(item: ShoppingListItem) {
        collection.insertOne(item)
    }

}


class DynamoDbShoppingListItemDao : ShoppingListItemDao, DynamoDbScope {
    private val client by lazy { DynamoDbAsyncClient.builder().build() }
    private val tableName = "shopping_list"

    override suspend fun getAllItems(): List<ShoppingListItem> {
        val scanRequest = ScanRequest.builder().tableName(tableName).build()
        return client.scan(scanRequest).await()!!.let {
            it.items().map { item ->
                ShoppingListItem(
                    desc = item.getValue("desc").s(),
                    priority = item.getValue("priority").n().toInt()
                )
            }
        }
    }

    override suspend fun deleteShoppingListItem(id: Int): Boolean {
        client.deleteItem {
            it.tableName(tableName)
            it.key(mutableMapOf("id" to id.attributeValue))
        }.await()

        return true
    }

    override suspend fun insertShoppingListItem(item: ShoppingListItem) {
        client.putItem {
            it.tableName(tableName)
            it.item(item.toDynamoItem())
        }.await()
    }

    private fun ShoppingListItem.toDynamoItem(): Map<String, AttributeValue> {
        return mapOf(
            "desc" to desc.attributeValue,
            "priority" to priority.attributeValue,
            "id" to id.attributeValue
        )
    }
}


object Database {
    private var dao: ShoppingListItemDao? = null


    fun dao(isDev: Boolean): ShoppingListItemDao {
        return dao ?: let {
            val newDao = if (isDev) {
                MongoShoppingListItemDao()
            } else {
                DynamoDbShoppingListItemDao()
            }
            dao = newDao
            newDao
        }
    }
}


val Application.shoppingListItemDao get() = Database.dao(isDev)
