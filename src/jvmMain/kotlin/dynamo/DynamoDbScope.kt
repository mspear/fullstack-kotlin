package com.virginpulse.dynamo

import software.amazon.awssdk.services.dynamodb.model.AttributeValue

interface DynamoDbScope {
    val Number.attributeValue: AttributeValue get() = AttributeValue.builder().n(this.toString()).build()
    val String.attributeValue: AttributeValue get() = AttributeValue.builder().s(this).build()
}
