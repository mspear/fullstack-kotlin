import kotlinx.css.*
import kotlinx.css.properties.*
import styled.StyleSheet

object ComponentStyle : StyleSheet("ComponentStyles") {

    val wrapper by css {
        display = Display.inlineBlock
        position = Position.fixed
        top = 50.pct
        left = 50.pct
        width = 20.pct
        transform {
            translate((-50).pct, (-50).pct)
        }
    }

    val flexbox by css {
        display = Display.flex

        children("div") {
            width = 100.px
            textAlign = TextAlign.center
            lineHeight = LineHeight("75px")
            fontSize = 30.px
            hover {
                backgroundColor = Color.lightPink
            }
        }

        children("div:not(:last-child)") {
            borderRight = "1px solid black"
        }
    }
}
