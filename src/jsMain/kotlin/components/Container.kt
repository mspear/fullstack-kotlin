@file:JsModule("react-bootstrap/Container")
@file:JsNonModule

package components

import react.RClass


@JsName("default")
external val Container: RClass<RBProps>

