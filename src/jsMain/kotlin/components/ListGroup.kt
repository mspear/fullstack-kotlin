@file:JsModule("react-bootstrap/ListGroup")
@file:JsNonModule

package components

import react.RClass
import react.RProps

@JsName("default")
external val ListGroup: RClass<RProps>

