package components

import EventHandler
import react.RClass
import react.RProps


external interface ListGroupItemProps : RProps {
    var action: Boolean
    var onClick: EventHandler
}

val ListGroupItem: RClass<ListGroupItemProps> = ListGroup.asDynamic().Item
