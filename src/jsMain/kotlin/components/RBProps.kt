package components

import react.RProps

external interface RBProps : RProps {
    var className: String
}
