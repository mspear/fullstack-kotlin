@file:JsModule("react-bootstrap/Row")
@file:JsNonModule

package components

import react.RClass

@JsName("default")
external val Row: RClass<RBProps>
