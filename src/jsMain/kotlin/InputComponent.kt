import kotlinx.html.InputType
import kotlinx.html.js.onChangeFunction
import kotlinx.html.js.onSubmitFunction
import org.w3c.dom.HTMLInputElement
import org.w3c.dom.events.Event
import react.*
import react.dom.*

external interface InputProps : RProps {
    var onSubmit: (String) -> Unit
}

typealias EventHandler = (Event) -> Unit

val InputComponent = functionalComponent<InputProps> { props ->
    val (text, setText) = useState("")

    val submitHandler: EventHandler = {
        it.preventDefault()
        setText("")
        props.onSubmit(text)
    }

    val changeHandler: EventHandler = {
        val value = (it.target as HTMLInputElement).value
        setText(value)
    }

    form {
        attrs.onSubmitFunction = submitHandler
        input(InputType.text) {
            attrs.onChangeFunction = changeHandler
            attrs.value = text
        }
    }
}

fun RBuilder.inputComponent(handler: InputProps.() -> Unit): ReactElement {
    return child(InputComponent) {
        this.attrs(handler)
    }
}
