import components.Container
import components.ListGroup
import components.ListGroupItem
import components.Row
import kotlinx.browser.document
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import react.*
import react.dom.*


private val scope = MainScope()

external interface AppState : RState {
    var shoppingList: List<ShoppingListItem>
}

class App : RComponent<RProps, AppState>() {

    override fun AppState.init() {
        shoppingList = emptyList()
    }

    override fun componentDidMount() {
        scope.launch {
            refreshShoppingList()
        }
    }

    override fun RBuilder.render() {
        Container {
            Row {
                h1 {
                    +"Full-Stack Shopping List"
                }
            }
            Row {
                ListGroup {
                    this@App.state.shoppingList.sortedByDescending(ShoppingListItem::priority).forEach { item ->
                        ListGroupItem {
                            attrs.action = true
                            attrs.onClick = {
                                it.preventDefault()
                                scope.launch {
                                    Api.deleteShoppingListItem(item)
                                    refreshShoppingList()
                                }
                            }
                            key = item.toString()
                            +"[${item.priority}] ${item.desc}"
                        }
                    }
                }
            }
            Row {
                attrs.className = "pt-3"
                inputComponent {
                    onSubmit = { input ->
                        val cartItem = ShoppingListItem(input.replace("!", ""), input.count { it == '!' })
                        scope.launch {
                            Api.addShoppingListItem(cartItem)
                            refreshShoppingList()
                        }
                    }
                }
            }
        }
    }

    private suspend fun refreshShoppingList() {
        val allItems = Api.getShoppingList()
        setState {
            shoppingList = allItems
        }
    }
}

fun RBuilder.app(handler: RProps.() -> Unit): ReactElement {
    return child(App::class) {
        this.attrs(handler)
    }
}


fun main() {
    kotlinext.js.require("bootstrap/dist/css/bootstrap.min.css")
    render(document.getElementById("root")) {
        app {}
    }
}


